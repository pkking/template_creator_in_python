'''
Created on 2012-10-23

@author: Administrator
'''
#coding = utf-8

import os
import shutil
import fnmatch

class template_creator(object):
    '''
    contain file_path, file_name, fill_sufix
    '''
    file_sufix = {'cpp':'.cpp','c':'.c','head':'.h'}

    def __init__(self, old_prefix, prefix):
        '''
        初始化要进行替换的文件名列表
        '''
        self.file_name = os.listdir(os.curdir)
        
        if str(old_prefix)+'.cpp' in self.file_name:
            self.file_name.remove(old_prefix + self.file_sufix['cpp'])
            self.file_name.append(prefix + self.file_sufix['cpp'])
        if str(old_prefix)+'.h' in self.file_name:
            self.file_name.remove(old_prefix + self.file_sufix['head'])
            self.file_name.append(prefix + self.file_sufix['head'])
        if str(old_prefix)+'.c' in self.file_name:
            self.file_name.remove(old_prefix + self.file_sufix['c'])
            self.file_name.append(prefix + self.file_sufix['c'])

    def copy_file_to_path(self, old_prefix, prefix):
        '''
        拷贝当前目录到当前目录下的prefix目录下,并更改文件名
        '''
        try:
            shutil.copytree('.', prefix)
        except WindowsError:
            print('dir is exist!')
        os.chdir('./'+prefix)
        name = os.listdir()
        if old_prefix + self.file_sufix['cpp'] in name:
            os.rename(old_prefix + self.file_sufix['cpp'], prefix +self.file_sufix['cpp'])
        if old_prefix + self.file_sufix['head'] in name:
            os.rename(old_prefix + self.file_sufix['head'], prefix + self.file_sufix['head'])
        if old_prefix + self.file_sufix['c'] in name:
            os.rename(old_prefix + self.file_sufix['c'], prefix + self.file_sufix['c'])
            
        print('dir copyed done !')
    def replace_prefix(self, old_prefix, prefix):
        '''
        替换目录中部分文件内的关键字
        '''
        print(self.file_name)
        for name in self.file_name:
            if(os.path.isfile(name) == True and(fnmatch.fnmatch(name, '*.cpp') == True
                                                or fnmatch.fnmatch(name,'*.c') == True
                                                or fnmatch.fnmatch(name,'*.h') == True)):
                print('replace '+name)
                fp_src = open(name, 'r')
                fp_dst = open(name + 'tmp', 'w', encoding = fp_src.encoding)
                print(name+' file successfully')
                while True:
                    try:
                        line = fp_src.read()
                    except UnicodeDecodeError:
                        print(name +'`s codec do not fit !')
                    line = line.replace(old_prefix, prefix)
                    if len(line) == 0:
                        break   
                    fp_dst.write(line)
                fp_src.close()
                fp_dst.close()
                try:
                    os.remove(name)
                    os.rename(name +'tmp', name)
                except WindowsError:
                    print('file '+name +' in used!')
                print(name +' convert completed!')
                os.system('pause')
        
old_prefix ='ES7481' #input('输入所在要替换的驱动名（如ES7481）：')
prefix ='ES1212' #input('输入要生成的驱动名：')
my_file = template_creator(old_prefix, prefix)
my_file.copy_file_to_path(old_prefix, prefix)
my_file.replace_prefix(old_prefix, prefix)
os.system('pause')

